const express = require('express');
const router = express.Router();
const video_controller = require('../controllers/video.controller');

router.get('/node/ping', video_controller.test);
router.get('/node/livecount/:id', video_controller.livecount);

router.post('/node/create', video_controller.video_create);
router.post('/node/delete', video_controller.video_delete);
module.exports = router;